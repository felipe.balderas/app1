const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/app1-db-app', {
        useCreateIndex: true,
        useNewUrlParser: true,
        useFindAndModify: false
    })
    .then(db => console.log("DB conectada"))
    .catch(err => console.log(err));