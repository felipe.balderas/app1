const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const User = require('../models/Users');

//autenticacion
passport.use(new LocalStrategy({
    usernameField: 'email',
}, async(email, password, done) => {
    const user = await User.findOne({ email: email });
    if (!user) {
        //primer dato es el error, si devuelve false no hay algun usuario y mensaje del callback
        return done(null, false, { message: 'Not User found.' });
    } else {
        //se encontro el user
        const match = await user.matchPassword(password);
        if (match) {
            //error, y user
            return done(null, user);
        } else {
            return done(null, false, { message: 'Incorrect Password' });
        }
    }
}));
//toma usuario y callback, sirve para almacenar usuarios
passport.serializeUser((user, done) => {
    done(null, user.id);
});
passport.deserializeUser((id, done) => {
    User.findById(id, (err, user) => {
        done(err, user);
    })
});