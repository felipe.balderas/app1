const router = require('express').Router();

//add model
const Note = require('../models/Notes');
//helper de autenticacion es un middleware
const { isAuthenticated } = require('../helpers/auth');

router.get('/notes/add', isAuthenticated, (req, res) => {
    res.render('notes/add');
});
//Se le pone async en el metodo post para que la cosa sea asyncrona
router.post('/notes/new-notes', isAuthenticated, async(req, res) => {
    const { title, description } = req.body;
    const errors = [];
    if (!title) {
        errors.push({ text: "Please Write a Title!" });
    }
    if (!description) {
        errors.push({ text: "Please witre a description!" });
    }
    if (errors.length > 0) {
        res.render('notes/add', {
            errors,
            title,
            description
        });
    } else {

        const newNote = new Note({ title, description });
        newNote.user = req.user.id; //enlazar la nota con el usuario
        await newNote.save(); //Guardar en DB asyncronamente

        req.flash('success_msg', 'Note Added Successfully!');

        res.redirect('/notes');
    }

}); //donde llegaran las notas nuevas

router.get('/notes', isAuthenticated, async(req, res) => {
    const notes = await Note.find({ user: req.user.id }).sort({ date: 'desc' });
    res.render('notes/all-notes', { notes, req });
});

router.get('/notes/edit/:id', isAuthenticated, async(req, res) => {
    const note = await Note.findById(req.params.id);
    res.render('notes/edit-note', { note });
});
router.put('/notes/edit-note/:id', isAuthenticated, async(req, res) => {
    const { title, description } = req.body;
    await Note.findByIdAndUpdate(req.params.id, { title, description });
    req.flash('success_msg', 'Note Updated Successfully');
    res.redirect('/notes');
});
router.delete('/notes/delete/:id', isAuthenticated, async(req, res) => {
    await Note.findByIdAndDelete(req.params.id);
    req.flash('success_msg', 'Note Deleted Successfully');
    res.redirect('/notes');
});
module.exports = router;